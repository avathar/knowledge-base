import Vue from 'vue'
import Vuex from 'vuex'
import knowledgeBase from "./knowledgeBase";
// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      knowledgeBase
      // example
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })
  if (process.env.DEV && module.hot) {
    module.hot.accept(['./knowledgeBase'], () => {
      const newKnowledgeBase = require('./knowledgeBase').default
      Store.hotUpdate({modules: {knowledgeBase: newKnowledgeBase}})
    })
  }

  return Store
}
